package com.example.finals

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_dessert.*

class dessertActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dessert)
        init()
    }
    private fun init(){
        brownieViewButton.setOnClickListener {
            val intent= Intent(this,raspberryBrownieActivity::class.java)
            startActivity(intent)
        }
        marshmallowViewButton.setOnClickListener {
            val intent=Intent(this, marshmallowActivity::class.java)
            startActivity(intent)
        }
    }
}