package com.example.finals

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_drinks.*

class drinksActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drinks)
        init()
    }
    private fun init(){
        mimosaViewButton.setOnClickListener {
            val intent= Intent(this,mimosaActivity::class.java)
            startActivity(intent)
        }
        meyerViewButton.setOnClickListener {
            val intent=Intent(this,meyerActivity::class.java)
            startActivity(intent)
        }
        eggnogViewButton.setOnClickListener {
            val intent=Intent(this, eggnogActivity::class.java)
            startActivity(intent)
        }
    }

}