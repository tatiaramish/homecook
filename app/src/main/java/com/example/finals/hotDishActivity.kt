package com.example.finals

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_hot_dish.*

class hotDishActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hot_dish)
        init()
    }
    private fun init(){
        soupViewButton.setOnClickListener {
            val intent= Intent(this, soupActivity::class.java)
            startActivity(intent)
        }
        beanViewButton.setOnClickListener {
            val intent=Intent(this,beansActivity::class.java)
            startActivity(intent)
        }
    }
}