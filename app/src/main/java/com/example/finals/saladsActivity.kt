package com.example.finals

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_salads.*

class saladsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_salads)
        init()

    }
    private fun init(){
        mandarinSaladViewB.setOnClickListener {
            val intent=Intent(this,mandarinSaladActivity::class.java)
            startActivity(intent)
        }
        farfalleView.setOnClickListener {
            val intent=Intent(this, farfalleActivity::class.java)
            startActivity(intent)
        }
    }
}