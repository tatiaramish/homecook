package com.example.finals

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init(){
        signInButton.setOnClickListener {
            signIn()
        }
        signUpButton.setOnClickListener {
            signUp()
        }
    }
    private fun signIn(){
        val intent=Intent(this, SignInActivity::class.java)
        startActivity(intent)
    }
    private fun signUp(){
        val intent=Intent(this,SignUpActivity::class.java)
        startActivity(intent)
    }
}