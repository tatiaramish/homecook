package com.example.finals

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_category.*

class CategoryActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_category)
        init()
    }
    private fun init(){
       saladButton.setOnClickListener {
           val intent=Intent(this,saladsActivity::class.java)
           startActivity(intent)
       }
        hotDishButton.setOnClickListener {
            val intent=Intent(this, hotDishActivity::class.java)
            startActivity(intent)
        }
        drinkButton.setOnClickListener {
              val intent=Intent(this,drinksActivity::class.java)
            startActivity(intent)
        }
        dessertButton.setOnClickListener {
             val intent=Intent(this,dessertActivity::class.java)
            startActivity(intent)
        }
    }
}